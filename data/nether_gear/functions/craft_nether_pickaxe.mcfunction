recipe take @s nether_gear:nether_pickaxe
clear @s minecraft:stone_pickaxe

give @s stone_pickaxe{display:{Name:'{"text":"Nether Pickaxe","italic":"false","color":"white"}'},Enchantments:[{id:efficiency,lvl:1},{id:unbreaking,lvl:1}],HideFlags:1}
advancement revoke @s only nether_gear:nether_pickaxe
