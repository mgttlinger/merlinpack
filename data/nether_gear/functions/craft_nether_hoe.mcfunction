recipe take @s nether_gear:nether_hoe
clear @s minecraft:stone_hoe

give @s stone_hoe{display:{Name:'{"text":"Nether Hoe","italic":"false","color":"white"}'},Enchantments:[{id:efficiency,lvl:1},{id:unbreaking,lvl:1}],HideFlags:1}
advancement revoke @s only nether_gear:nether_hoe
