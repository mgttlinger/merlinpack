recipe take @s nether_gear:nether_shovel
clear @s minecraft:stone_shovel

give @s stone_shovel{display:{Name:'{"text":"Nether Shovel","italic":"false","color":"white"}'},Enchantments:[{id:efficiency,lvl:1},{id:unbreaking,lvl:1}],HideFlags:1}
advancement revoke @s only nether_gear:nether_shovel
