recipe take @s nether_gear:nether_axe
clear @s minecraft:stone_axe

give @s stone_axe{display:{Name:'{"text":"Nether Axe","italic":"false","color":"white"}'},Enchantments:[{id:efficiency,lvl:1},{id:unbreaking,lvl:1}],HideFlags:1}
advancement revoke @s only nether_gear:nether_axe
